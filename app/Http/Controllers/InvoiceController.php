<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class InvoiceController extends Controller
{
    const PAGINATION_DEFAULT_LIMIT = 10;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }    
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int)$request->limit ? (int)$request->limit : $this::PAGINATION_DEFAULT_LIMIT;

        try {
            $invoices = Invoice::paginate($limit);
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 404);
        }

        return response()->json($invoices, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'user_id' => 'bail|required|integer|min:1',
            'title' => 'bail|required|max:255',
            'invoice_recipient' => 'bail|required',
            'invoice_sender' => 'bail|required',
            'payment_deadline' => 'bail|required|date',
            'total_tax_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'total_net_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'total_gross_amount' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $invoice = new Invoice();
        $invoice->user_id = Auth::id();//$request->user_id;
        $invoice->title = $request->title;
        $invoice->invoice_recipient = $request->invoice_recipient;
        $invoice->invoice_sender = $request->invoice_sender;			
        $invoice->payment_deadline = $request->payment_deadline;
        $invoice->total_tax_amount = $request->total_tax_amount;
        $invoice->total_net_amount = $request->total_net_amount;
        $invoice->total_gross_amount = $request->total_gross_amount;		

        try {		
            $invoice->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
		
        return response()->json($invoice, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return Invoice::with('items')->find($invoice->id);
    }
	
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function getUserId()
    {
        return Auth::id();
    }*/	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'bail|required|integer|min:1',
            'title' => 'bail|required|max:255',
            'invoice_recipient' => 'bail|required',
            'invoice_sender' => 'bail|required',
            'payment_deadline' => 'bail|required|date',
            'total_tax_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'total_net_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'total_gross_amount' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        if ($validator->fails() || Auth::id() != $request->user_id) {
            return response()->json(['error' => $validator->errors()], 400);
        }
		
        $invoice->user_id = $request->user_id;
        $invoice->title = $request->title;
        $invoice->invoice_recipient = $request->invoice_recipient;
        $invoice->invoice_sender = $request->invoice_sender;			
        $invoice->payment_deadline = $request->payment_deadline;
        $invoice->total_tax_amount = $request->total_tax_amount;
        $invoice->total_net_amount = $request->total_net_amount;
        $invoice->total_gross_amount = $request->total_gross_amount;		

        try {
            $invoice->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
		
        return response()->json($invoice, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        if (Auth::id() != $invoice->user_id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }
		
        try {
            Invoice::destroy($invoice->id);
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(null, 204);
    }
}
