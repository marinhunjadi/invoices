<?php

namespace App\Http\Controllers;

use App\Item;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class ItemController extends Controller
{
    //const PAGINATION_DEFAULT_LIMIT = 10;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }    
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$limit = (int)$request->limit ? (int)$request->limit : $this::PAGINATION_DEFAULT_LIMIT;

        try {
            $items = Item::paginate($limit);
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 404);
        }*/
		
        $items = Item::where('invoice_id', $request->get('invoice_id'))->get();

        return response()->json($items, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'bail|required|integer|min:1',
            'invoice_id' => 'bail|required|integer|min:1',
            'title' => 'bail|required|max:255',
            'quantity' => 'bail|required|integer',
            'unit_price' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'tax_rate' => 'bail|nullable|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'net_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'gross_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'tax_amount' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        if ($validator->fails() || Auth::user()->id != $request->user_id) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $item = new Item();
        $item->invoice_id = $request->invoice_id;
        $item->title = $request->title;
        $item->quantity = $request->quantity;
        $item->unit_price = $request->unit_price;			
        $item->tax_rate = $request->tax_rate;
        $item->net_amount = $request->net_amount;
        $item->gross_amount = $request->gross_amount;
        $item->tax_amount = $request->tax_amount;		
        
        try {
		    $item->save();
            //$items = Item::where('invoice_id', $request->get('invoice_id'))->get();
            $invoice = Invoice::with('items')->find($request->invoice_id);
            $invoice->total_tax_amount = 0;
            $invoice->total_net_amount = 0;
            $invoice->total_gross_amount = 0;			
            foreach($invoice->items as $it){
                $invoice->total_tax_amount += $it->tax_amount;
                $invoice->total_net_amount += $it->net_amount;
                $invoice->total_gross_amount += $it->gross_amount;			
            }
            $invoice->save();			
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
		
        return response()->json($item, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return $item;
    }
	
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserId()
    {
        return Auth::id();
    }	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            //'user_id' => 'bail|required|integer|min:1',
            'invoice_id' => 'bail|required|integer|min:1',
            'title' => 'bail|required|max:255',
            'quantity' => 'bail|required|integer',
            'unit_price' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'tax_rate' => 'bail|nullable|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'net_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'gross_amount' => 'bail|required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'tax_amount' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        if ($validator->fails() /*|| Auth::id() != $request->user_id*/) {
            return response()->json(['error' => $validator->errors()], 400);
        }
		
        $item->invoice_id = $request->invoice_id;
        $item->title = $request->title;
        $item->quantity = $request->quantity;
        $item->unit_price = $request->unit_price;			
        $item->tax_rate = $request->tax_rate;
        $item->net_amount = $request->net_amount;
        $item->gross_amount = $request->gross_amount;
        $item->tax_amount = $request->tax_amount;

        try {
            $item->save();
            //$items = Item::where('invoice_id', $request->get('invoice_id'))->get();
            $invoice = Invoice::with('items')->find($request->invoice_id);
            $invoice->total_tax_amount = 0;
            $invoice->total_net_amount = 0;
            $invoice->total_gross_amount = 0;			
            foreach($invoice->items as $it){
                $invoice->total_tax_amount += $it->tax_amount;
                $invoice->total_net_amount += $it->net_amount;
                $invoice->total_gross_amount += $it->gross_amount;			
            }
            $invoice->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
		
        return response()->json($item, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $invoice = Invoice::with('items')->find($item->invoice_id);
        if (Auth::id() != $invoice->user_id) {
            return response()->json(['error' => $validator->errors()], 400);
        }
		DB::beginTransaction();
        try {		    
            Item::destroy($item->id);
            //$items = Item::where('invoice_id', $request->get('invoice_id'))->get();
            //$invoice = Invoice::with('items')->find($item->invoice_id);
            $items = $invoice->items;
            $items = $items->keyBy('id');
            $items->forget($item->id);
            $invoice->total_tax_amount = 0;
            $invoice->total_net_amount = 0;
            $invoice->total_gross_amount = 0;			
            foreach($items as $it){
                $invoice->total_tax_amount += $it->tax_amount;
                $invoice->total_net_amount += $it->net_amount;
                $invoice->total_gross_amount += $it->gross_amount;			
            }
            $invoice->save();
            DB::commit();			
        } catch(\Exception $e) {
		    DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(null, 204);
    }
}
