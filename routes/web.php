<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Invoices
Route::resource('/api/invoices', 'InvoiceController');

// Items
Route::resource('/api/items', 'ItemController');
Route::get('/api/userid', 'ItemController@getUserId');

Route::fallback(function(){
    //return response()->json(['message' => 'Not Found!'], 404);
    return response('Not Found');
});
