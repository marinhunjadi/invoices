
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import App from './App.vue';

//import Example from './components/ExampleComponent.vue';
//import Home from './components/HomeComponent.vue';
import ListInvoices from './components/ListInvoices.vue';
//import ShowInvoice from './components/ShowInvoice.vue';
import CreateInvoice from './components/CreateInvoice.vue';
import EditInvoice from './components/EditInvoice.vue';
import ListItems from './components/ListItems.vue';
//import ShowItem from './components/ShowItem.vue';
import CreateItem from './components/CreateItem.vue';
import EditItem from './components/EditItem.vue';

const routes = [
  /*{
    name: 'Example',
    path: '/Example',
    component: Example
  },
  {
    name: 'Home',
    path: '/home',
    component: Home
  },*/
  {
    name: 'ListInvoices',
    path: '/home',	
    component: ListInvoices
  },
  /*{
    name: 'ShowInvoice',
    path: '/invoices/show/:id',	
    component: ShowInvoice
  },*/
  {
    name: 'EditInvoice',
    path: '/invoices/edit/:id',	
    component: EditInvoice
  },
  {
    name: 'CreateInvoice',
    path: '/invoices/create',	
    component: CreateInvoice
  },
  /*{
    name: 'ListItems',
    path: '/items',	
    component: ListItems
  },*/
  /*{
    name: 'ShowItem',
    path: '/items/show/:id',	
    component: ShowItem
  },*/
  {
    name: 'EditItem',
    path: '/items/edit/:id',	
    component: EditItem
  },
  {
    name: 'CreateItem',
    path: '/items/create/:id',	
    component: CreateItem
  },  
];

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({ mode: 'history', routes: routes});
if(!!document.getElementById("app-component"))
new Vue(Vue.util.extend({ router }, App)).$mount('#app-component');
