@extends('layouts.app')

@section('content')
@if (Auth::check())
<div id="app-component"></div>
@else
<script>
window.location.href = "/login";
</script>
@endif
@endsection
